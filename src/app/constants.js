(function () {
  'use strict';

  angular
    .module('beehive')

    .constant('moment', moment)

    .constant('$', jQuery);

}());