(function () {
  'use strict';

  angular
    .module('beehive')

    .constant('API_URL', '__${rest_url}__/api');
}());