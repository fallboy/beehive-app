(function () {
  'use strict';

  angular
    .module('beehive')
    .factory('MessageResource', MessageResource);

  function MessageResource($resource, API_URL) {

    return $resource(API_URL + '/message', {}, {

      send: {
        method: 'PUT'
      }
    });
  }
}());