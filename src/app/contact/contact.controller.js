(function () {
  'use strict';

  angular
    .module('beehive')
    .controller('ContactCtrl', ContactCtrl);

  function ContactCtrl(MessageResource) {
    var vm = this;

    //TODO: Add csrf protection to all forms!

    vm.sent = false;
    vm.waiting = false;

    vm.send = function () {
      if (vm.ctForm.$valid) {

        vm.waiting = true;

        vm.ct.messageDate = new Date();

        MessageResource.send(vm.ct, function () {
          vm.sent = true;
            vm.waiting = false;
        },
          function (err) {
            vm.err  = 'Sorry - It appears we\'re having technical difficulties. We\'ll get it figured out.  In the meantime please reach out to us on facebook.';
          });

      }
    }
  }
}());