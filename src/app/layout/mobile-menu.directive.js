(function () {
  'use strict';

  angular
    .module('beehive')
    .directive('mobileMenu', MobileMenu);

  function MobileMenu() {
    
    return {
      restrict: 'E',
      transclude: true,
      templateUrl: 'mobile-menu.html',
      controller: MobileMenuCtrl,
      controllerAs: 'mobi',
      scope: true,
      bindToController: {
        menuItems: '='
      }
    };
  }

  function MobileMenuCtrl() {
    var vm = this;
  }
}());