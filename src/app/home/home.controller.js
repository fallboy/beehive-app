(function () {
  'use strict';

  angular
    .module('beehive')
    .controller('HomeCtrl', HomeCtrl);

  function HomeCtrl(CategoryStore) {
    var vm = this;

    CategoryStore.getCategories().then(function (cats) {
      vm.categories = cats;
    });

    vm.noop = function (evt) {
      console.log(evt);
      evt.preventDefault();
    };

  }
}());