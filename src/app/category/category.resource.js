(function () {
  'use strict';

  angular
    .module('beehive')
    .factory('CategoryResource', CategoryResource);

  function CategoryResource($resource, API_URL) {
    return $resource(API_URL + '/categories', {}, {
      list: {
        isArray: true,
        method: 'GET'
      }
    });
  }
}());