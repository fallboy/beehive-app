(function () {
  'use strict';

  angular
    .module('beehive')
    .controller('CategoryCtrl', CategoryCtrl);

  function CategoryCtrl($stateParams, $location, CategoryStore, ItemResource) {
    var vm = this;

    CategoryStore.promise.then(function () {
      vm.category = CategoryStore.findCategory($stateParams.category);

      //redirect if category doesn't exist
      if(!vm.category) {
        $location.path('/');
      }


      vm.items = ItemResource.listCategoryItems({categoryId: vm.category._id});


    });


    vm.imgBgStyle = function (item) {
      var st = {
        'background-image': 'url('+ item.imgUrl +')'
      };
      console.log(st);

      return st;

    }
  }
}());