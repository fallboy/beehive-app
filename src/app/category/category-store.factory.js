(function () {
  'use strict';

  angular
    .module('beehive')
    .factory('CategoryStore', CategoryStore);

  function CategoryStore(CategoryResource, $q) {
    var categories = [];
    var segmentMap = {};
    var deferred = $q.defer();
    var promise = deferred.promise;

    function cleanCategories(cats) {
      return cats.map(function (cat) {
        cat.value = cat.name;
        cat.urlSeg = cat.value.replace(/\s/g, '_').replace(/\//g, '-');
        segmentMap[cat.urlSeg] = cat;
        return cat;
      });
    }

    function getCats() {
      if (!categories.length) {
        CategoryResource.list(function (cats) {
          categories = cleanCategories(cats);
          deferred.resolve(categories);
        });
      }

      return promise;
    }

    function findCategory(segment) {
      if(categories.length){
        return segmentMap[segment];
      }

    }


    return {
      getCategories: getCats,
      findCategory: findCategory,
      promise: promise
    }
  }
}());