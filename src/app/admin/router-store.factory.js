(function () {
  'use strict';

  angular
    .module('beehive')
    .factory('RouterStore', RouterStore);

  function RouterStore($location) {

    var previous;

    return {
        forceAuth: function () {
        previous = $location.path();
        $location.path('/authenticate');
      },

      reRoute: function () {
        $location.path('/admin');
      }
    }
  }
}());