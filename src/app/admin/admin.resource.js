(function () {
  'use strict';

  angular
    .module('beehive')
    .factory('AdminResource', AdminResource);

  function AdminResource($resource, API_URL, RouterStore, TokenStore) {

    function processErr(res) {
      if (res.status === 401 || res.status === 403) {
        RouterStore.forceAuth();
      }
    }

    return {
      //do this so we can set token at runtime
      get: function () {


        return $resource(API_URL + '/admin', {}, {

          addCategory: {
            method: 'PUT',
            url: API_URL + '/admin/category',
            interceptor: {
              responseError: processErr
            },
            headers: {'x-access-token': TokenStore.get()}
          },

          addItem: {
            method: 'PUT',
            url: API_URL + '/admin/item',
            interceptor: {
              responseError: processErr
            },
            headers: {'x-access-token': TokenStore.get()}
          },

          deleteItem: {
            method: 'DELETE',
            url: API_URL + '/admin/item/delete/:id',
            interceptor: {
              responseError: processErr
            },
            headers: {'x-access-token': TokenStore.get()}
          },

          messages: {
            method: 'GET',
            url: API_URL + '/admin/messages',
            isArray: true,
            interceptor: {
              responseError: processErr
            },
            headers: {'x-access-token': TokenStore.get()}
          },

          allEvents: {
            method: 'GET',
            isArray: true,
            url: API_URL + '/admin/events/all',
            interceptor: {
              responseError: processErr
            },
            headers: {'x-access-token': TokenStore.get()}
          },

          addEvent: {
            method: 'PUT',
            url: API_URL + '/admin/event',
            interceptor: {
              responseError: processErr
            },
            headers: {'x-access-token': TokenStore.get()}
          },

          deleteEvent: {
            method: 'DELETE',
            url: API_URL + '/admin/event/delete/:id',
            interceptor: {
              responseError: processErr
            },
            headers: {'x-access-token': TokenStore.get()}
          },

          deleteMessage: {
            method: 'DELETE',
            url: API_URL + '/admin/message/delete/:id',
            interceptor: {
              responseError: processErr
            },
            headers: {'x-access-token': TokenStore.get()}
          }

        });

      }
    };
  }
}());