(function () {
  'use strict';

  angular
    .module('beehive')
    .controller('AdminCtrl', AdminCtrl);

  function AdminCtrl(CategoryResource, ItemResource, AdminResource, $timeout, $window, $location, moment, TokenStore) {
    var vm = this;

    vm.logout = function () {
      TokenStore.delete();
      $location.path('/');
    };

    vm.categories = CategoryResource.list();
    vm.items = ItemResource.query();
    vm.events = AdminResource.get().allEvents();
    vm.messages = AdminResource.get().messages();

    vm.item = {};
    vm.event = {};
    vm.category = {};
    vm.optionModel = [];

    vm.formatDate = function (dt) {
      return moment(dt).format("dddd, MMMM Do YYYY");
    };

    vm.addOption = function () {
      vm.optionModel.push('');
    };

    function cleanOptionModel() {
      var cleaned = vm.optionModel.map(function (entry) {
        entry = entry.trim();
        if (entry.length) return entry;
      });

      vm.optionModel = [];
      if (cleaned.length) {
        return cleaned;
      }
    }

    vm.saveItem = function () {
      if (vm.itemForm.$valid) {
        vm.item.options = cleanOptionModel();
        AdminResource.get().addItem(vm.item, function (res) {
          vm.items.push(res);
          vm.item = {};
          vm.err = undefined;
          vm.success = 'Item Saved!';
          vm.itemForm.$setPristine();

          $location.path('/admin');

          $timeout(function () {
            vm.success = undefined;
          }, 5000);

        }, function (err) {
          vm.err = err;
          vm.sucess = undefined;
        });
      }

    };


    vm.saveEvent = function () {
      if (vm.eventForm.$valid) {

        AdminResource.get().addEvent(vm.event, function (res) {
            vm.events.push(res);
            vm.event = {};
            vm.err = undefined;
            vm.success = 'Event Saved!';
            vm.eventForm.$setPristine();

            $location.path('/admin');

            $timeout(function () {
              vm.success = undefined;
            }, 5000);


          }, function (err) {
            vm.err = err;
            vm.success = undefined;
          }
        );

      }

    };


    vm.saveCategory = function () {

      if (vm.catForm.$valid) {
        if ($window.confirm('Categories cannot be removed once created.  Are you sure you want to add this category?')) {

          AdminResource.get().addCategory(vm.category, function (res) {
            vm.categories.push(res);
            vm.category = {};
            vm.success = 'Category "' + res.name + '" saved!';
            vm.err = undefined;
            vm.catForm.$setPristine();

            $location.path('/admin');

            $timeout(function () {
              vm.success = undefined;
            }, 5000);
          }, function (err) {
            vm.err = err;
            vm.success = undefined;
          });
        }
      }
    };


    vm.deleteItem = function (item) {
      if ($window.confirm('Are you sure you want to delete this item?')) {

        AdminResource.get().deleteItem({id: item._id},
          function () {
            vm.items = ItemResource.query();
            vm.success = 'Item Deleted.';
            $timeout(function () {
              vm.success = undefined;
            }, 5000);

          }, function (err) {
            vm.err = err;
            vm.success = undefined;
          });
      }
    };


    vm.deleteEvent = function (event) {
      if ($window.confirm('Are you sure you want to delete this event?')) {

        AdminResource.get().deleteEvent({id: event._id}, function () {

          vm.events = AdminResource.get().allEvents();
          vm.success = 'Event Deleted.';

          $timeout(function () {
            vm.success = undefined;
          }, 5000);

        }, function (err) {
          vm.err = err;
          vm.success = undefined;
        });

      }
    };


    vm.deleteMessage = function (message) {
      if ($window.confirm('Are you sure you want to delete this message?')) {

        AdminResource.get().deleteMessage({id: message._id}, function () {

          vm.messages = AdminResource.get().messages({token: TokenStore.get()});
          vm.success = 'Message Deleted.';

          $timeout(function () {
            vm.success = undefined;
          }, 5000);


        }, function (err) {
          vm.err = err;
          vm.success = undefined;
        });

      }
    };


    vm.findCategoryAttributeById = function (id, att) {
      var found = vm.categories.find(function (cat) {
        return cat._id === id;
      });

      if (found) return found[att];
    };


  }
}());