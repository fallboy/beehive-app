(function () {
  'use strict';

  angular
    .module('beehive')
    .controller('MainCtrl', MainCtrl);

  function MainCtrl(CategoryStore, $location) {
    var vm = this;

    vm.categories = [{
      value: 'Home',
      urlSeg: ''
    }];

    var endMenu = [
      {
        value: 'Events',
        separator: true,
        urlSeg: 'Events'
      }, {
        value: 'Shipping',
        urlSeg: 'Shipping'
      }, {
        value: 'Contact',
        urlSeg: 'Contact'
      }
    ];

    CategoryStore.getCategories().then(function (cats) {
      vm.categories = vm.categories.concat(cats).concat(endMenu);
    });

    vm.isActiveLink = function (item) {
      return '/' + item.urlSeg === $location.path() ? 'active' : '';
    };



  }
}());