(function () {
  'use strict';

  angular
    .module('beehive')
    .factory('TokenStore', TokenStore);

  function TokenStore($cookies) {

    var sessionToken;

    return {
      get: function () {
        return sessionToken || $cookies.get('token');
      },
      put: function (token) {
        sessionToken = token;
        $cookies.put('token', token);
      },
      delete: function () {
        sessionToken = undefined;
        $cookies.remove('token');
      }
    }
  }
}());