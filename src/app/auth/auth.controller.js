(function () {
  'use strict';

  angular
    .module('beehive')
    .controller('AuthCtrl', AuthCtrl);

  function AuthCtrl(AuthResource, RouterStore, TokenStore) {
    var vm = this;

    vm.waiting = false;

    vm.authenticate = function () {
      if (vm.authForm.$valid) {
        vm.waiting = true;
        AuthResource.save(vm.auth, function (res) {

          TokenStore.put(res.token);
          RouterStore.reRoute();
          vm.waiting = false;


        }, function (err) {
          console.error(err.status);
          if (err.status === 403) {
            vm.err = 'Invalid Login';
          } else {
            vm.err = err;
          }
        });
      }
    };
  }
}());