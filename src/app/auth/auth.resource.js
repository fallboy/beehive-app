(function () {
  'use strict';

  angular
    .module('beehive')
    .factory('AuthResource', AuthResource);

  function AuthResource($resource, API_URL) {

    return $resource(API_URL + '/authenticate');
  }
}());