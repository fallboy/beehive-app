(function () {
  'use strict';

  angular
    .module('beehive')
    .factory('ItemResource', ItemResource);

  function ItemResource($resource, API_URL) {
    return $resource(API_URL + '/items', {}, {

      listCategoryItems: {
        method: 'GET',
        isArray: true,
        url: API_URL + '/categories/:categoryId/items',
        cache: true
      }
    });
  }
}());