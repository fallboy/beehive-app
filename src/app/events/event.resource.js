(function () {
  'use strict';

  angular
    .module('beehive')
    .factory('EventResource', EventResource);

  function EventResource($resource, API_URL) {
    return $resource(API_URL + '/events');
  }
}());