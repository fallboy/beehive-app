(function () {
  'use strict';

  angular
    .module('beehive')
    .controller('EventCtrl', EventCtrl);

  function EventCtrl(EventResource, moment) {
    var vm = this;

    vm.events = EventResource.query();

    vm.formatDate = function (dt) {
      return moment(dt).format("dddd, MMMM Do YYYY");
    }

  }
}());