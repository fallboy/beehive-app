(function () {
  'use strict';

  var app = angular
    .module('beehive', ['ngResource', 'ui.router', 'ngMessages', 'ngCookies']);

  app.config(Router);

  function Router($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');


    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })

      .state('authenitcate', {
        url: '/authenticate',
        templateUrl: 'auth.html',
        controller: 'AuthCtrl',
        controllerAs: 'act'
      })

      .state('admin', {
        url: '/admin',
        templateUrl: 'admin.html',
        controller: 'AdminCtrl',
        controllerAs: 'adm',
        resolve: {
          events: function (AdminResource) {
            return AdminResource.get().allEvents();
          }
        }
      })

      .state('admin.items', {
        url: '/items',
        templateUrl: 'admin-items.html'
      })

      .state('admin.new-item', {
        url: '/item/new',
        templateUrl: 'admin-add-item.html'
      })

      .state('admin.new-category', {
        url: '/category/new',
        templateUrl: 'admin-add-category.html'
      })

      .state('admin.events', {
        url: '/events',
        templateUrl: 'admin-events.html'
      })

      .state('admin.new-event', {
        url: '/event/new',
        templateUrl: 'admin-add-event.html'
      })

      .state('admin.messages', {
        url: '/messages',
        templateUrl: 'admin-messages.html'
      })

      .state('shipping', {
        url: '/Shipping',
        templateUrl: 'shipping.html'
      })

      .state('contact', {
        url: '/Contact',
        templateUrl: 'contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'contact'
      })

      .state('events', {
        url: '/Events',
        templateUrl: 'event.html',
        controller: 'EventCtrl',
        controllerAs: 'evt'
      })

      .state('category', {
        url: '/:category',
        templateUrl: 'category.html',
        controller: 'CategoryCtrl',
        controllerAs: 'CatCtrl'
      });

  }
}());