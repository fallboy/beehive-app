var gulp = require('gulp');
var concat = require('gulp-concat');
var path = require('path');
var less = require('gulp-less');
var browserSync = require('browser-sync').create();
var merge = require('merge-stream');
var replace = require('gulp-replace');
var templateCache = require('gulp-angular-templatecache');
var annotate = require('gulp-ng-annotate');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');

//var gulpConf = require('./gulp.conf.dev.json');
var gulpConf = require('./gulp.conf.json');


//var modulesDir = path.join(__dirname, 'node_modules');


gulp.task('vendor:js', function () {
  return gulp.src(gulpConf.paths.vendor.js)
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('dist/js'));
});


gulp.task('vendor:styles', function () {
  return gulp.src(gulpConf.paths.vendor.css)
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('dist/style'));
});

/**
 * copy vendor libs to public dir
 */
gulp.task('vendor', ['vendor:styles', 'vendor:js']);


gulp.task('styles', function () {
  return gulp.src(['src/styles/**/*.less', 'src/app/**/*.less'])
    .pipe(less())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(concat('style.min.css'))
    .pipe(sourcemaps.init())
    .pipe(cssmin())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/style'));
});


gulp.task('js', function () {
  var mainStream = gulp.src('src/**/*.js');

  //todo: add option for prod
  gulpConf.replace.prod.forEach(function (val) {
    mainStream.pipe(replace(val.from, val.to));
  });

  var directiveStream = gulp.src('src/app/**/*.html')
    .pipe(
    templateCache('templates', {
      module: 'beehive', base: function (file) {
        return /[^/]*$/.exec(file.relative)[0];
      }
    }));

  return merge(mainStream, directiveStream)
    .pipe(concat('app.min.js'))
    .pipe(annotate())
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));

});


/**
 * html
 */
gulp.task('html', function () {
  return gulp.src('src/index.html')
    .pipe(gulp.dest('dist'));
});

/**
 * assets
 */
gulp.task('assets', function () {
  return merge(
    gulp.src('node_modules/bootstrap/fonts/**/*', {base: 'node_modules/bootstrap'}),
    gulp.src('node_modules/font-awesome/fonts/**/*', {base: 'node_modules/font-awesome'}),
    gulp.src('src/img/**/*', {base: 'src'})
  )
    .pipe(gulp.dest('dist'));
});


/**
 * Browser-sync
 */
gulp.task('html-sync', ['html'], browserSync.reload);
gulp.task('style-sync', ['styles'], browserSync.reload);
gulp.task('js-sync', ['js'], browserSync.reload);


gulp.task('browser-sync', ['build'], function () {

  browserSync.init(['./dist/js/**/*.js', './dist/style/**/*.css', './dist/index.html'], {
    server: {
      baseDir: './dist/'
    },
    port: 3030
  });

  gulp.watch('src/index.html', ['html-sync']);
  gulp.watch('src/**/*.less', ['style-sync']);
  gulp.watch(['src/**/*.js', 'src/app/**/*.html'], ['js-sync']);

});


/**
 * Main Build
 */
gulp.task('build', ['vendor', 'styles', 'js', 'html', 'assets']);